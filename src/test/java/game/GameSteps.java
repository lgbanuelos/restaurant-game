package game;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameSteps {
	Restaurant restaurant = null;
	
	@Given("^the restaurant reputation is (\\d+)$")
	public void the_restaurant_reputation_is(int reputation) throws Throwable {
		restaurant = new Restaurant();
		restaurant.setReputation(reputation);
	}

	@Given("^(\\d+) clients are (\\w*)satisfied with the (\\w+)$")
	public void clients_are_satisfied_with_the_food(int numberOfClients, String prefix, String matter) throws Throwable {
		matter = matter.toLowerCase();
		for (int i = 0; i < numberOfClients; i++) {
			if (prefix.isEmpty()) {
				if (matter.startsWith("food"))
					restaurant.recordClientSatisfactionOnFood();
				else if (matter.startsWith("beverage"))
					restaurant.recordClientSatisfactionOnBeverage();
				else
					restaurant.recordClientSatisfactionOnService();
			} else {
				if (matter.startsWith("food"))
					restaurant.recordClientDissatisfactionOnFood();
				else if (matter.startsWith("beverage"))
					restaurant.recordClientDissatisfactionOnBeverage();
				else
					restaurant.recordClientDissatisfactionOnService();

			}
				
		}
	}

	@When("^the restaurant reputation is updated$")
	public void the_restaurant_reputation_is_updated() throws Throwable {
		restaurant.updateReputation();
	}

	@Then("^the restaurant reputation becomes (\\d+)$")
	public void the_restaurant_reputation_becomes(int reputation) throws Throwable {
		Assert.assertEquals(reputation, restaurant.getReputation());
	}
}
